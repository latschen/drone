package de.codecentric.flight;

import de.codecentric.flight.drones.ThreeSensorDrone;
import de.codecentric.flight.drones.TwoSensorDrone;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * creates the world with all its obstacles for the drone to flight inside
 */
public class WorldPanel extends JPanel implements Runnable {

    static final int WORLD_SIZE_X = 1800;
    static final int WORLD_SIZE_Y = 1000;


    /* true if the movement of the drone should be visualized as shadows */
    private final boolean showShadows = true;

    /* time in ms between the movement steps */
    private final long repaintInterval = 10;

    /* density of the obstacles, value from 0 (only borders) to 3 (very dense) */
    private final int obstascleDensity = 3;

    /* list of all obstacles */
    public static ArrayList<Rectangle> obstacles = new ArrayList<>();

    /* the drone to flight around inside */
    private AbstractDrone drone;

    public WorldPanel() {

        setBackground(Color.lightGray);

        /* world boundaries */
        obstacles.add(new Rectangle(0, 0, WORLD_SIZE_X, 10));
        obstacles.add(new Rectangle(0, 0, 10, WORLD_SIZE_Y));
        obstacles.add(new Rectangle(WORLD_SIZE_X-10, 0, 10, WORLD_SIZE_Y));
        obstacles.add(new Rectangle(0, WORLD_SIZE_Y-40, WORLD_SIZE_X, 10));

        /* create obstacles */
        if(obstascleDensity > 0) {

            obstacles.add(new Rectangle(400, 0, 200, 500));
            obstacles.add(new Rectangle(0, 700, 600, 80));
            obstacles.add(new Rectangle(900, 500, 200, 270));
            obstacles.add(new Rectangle(1500, 300, 50, 50));
        }

        if(obstascleDensity > 1) {
            obstacles.add(new Rectangle(900, 500, 600, 80));
            obstacles.add(new Rectangle(1100, 0, 20, 300));
        }

        if(obstascleDensity > 2) {
            obstacles.add(new Rectangle(100, 400, 20, 20));
            obstacles.add(new Rectangle(400, 600, 30, 20));
            obstacles.add(new Rectangle(800, 200, 50, 50));
            obstacles.add(new Rectangle(1300, 400, 50, 70));
            obstacles.add(new Rectangle(1600, 70, 120, 30));
            obstacles.add(new Rectangle(1400, 800, 50, 60));
            obstacles.add(new Rectangle(750, 820, 20, 20));
            obstacles.add(new Rectangle(200, 500, 50, 50));
        }

        drone = new ThreeSensorDrone(100.0, 100.0);
        add(drone);

        Thread t = new Thread(this);
        t.start();
    }


    @Override
    protected void paintComponent(Graphics g) {
        if(!showShadows) {
            super.paintComponent(g);
        }

        g.setColor(Color.DARK_GRAY);
        for(Rectangle r : obstacles) {
            g.fillRect(r.x, r.y, r.width, r.height);
        }

        /* let the drone draw herself */
        drone.paintComponent(g);
    }


    @Override
    public void run() {

        while(true) {
            try {
                Thread.sleep(repaintInterval);
                repaint();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
