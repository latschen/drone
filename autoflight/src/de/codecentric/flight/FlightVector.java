package de.codecentric.flight;

/**
 * Representation of a flight vector of the drone with x- and y-delta
 */
public class FlightVector {

    public static final int DIRECTION_LEFT = 1;
    public static final int DIRECTION_RIGHT = 2;

    public static final int BREAK = 3;
    public static final int ACCELERATE = 4;

    public static FlightVector[] flightVectors = new FlightVector[360];

    static {

        /* create a vector for all degrees */
        for(int i = 0; i <= 359; i = i + 1) {
            flightVectors[i] = new FlightVector(i);
            flightVectors[i].calculateStrafeVectors();
            System.out.println(flightVectors[i]);
        }
    }

    /* the mathematical degree value of that vector */
    public int degrees;

    /* the vector parameters */
    public double x;
    public double y;

    /* the strafe vector for this vector (90 degrees) */
    public FlightVector strafeVector;


    public FlightVector(int degrees) {
        this.degrees = degrees;

        /* transform radian value in degrees to x and y coordinates */
        double xIn = Math.cos(Math.toRadians((double)degrees));
        double yIn = Math.sin(Math.toRadians((double)degrees));

        /* calculate the length of the current vector */
        double vectorLength = Math.sqrt((xIn*xIn)+(yIn*yIn));

        /* normalize the vector on length = 1 */
        x = xIn / vectorLength;
        y = yIn / vectorLength;
    }


    /**
     * calculate the strafe vectors for a given upfront vector
     */
    private void calculateStrafeVectors() {
        strafeVector = new FlightVector(degrees + 90);
    }


    @Override
    public String toString() {
        return "Grad: " + degrees + " | Vektor: ( " + x + " | " + y + " )";
    }


    /**
     * get the flight vector for any angel by degrees
     * @param degrees
     * @return the flightvector for the given angle
     */
    public static FlightVector getFlightVectorByAngle(int degrees) {

        int returnId = degrees;

        /* negative degrees will be filled up until they are between 0 and 360 degrees */
        if(degrees < 0) {

            while(returnId < 0) {
                returnId = returnId + flightVectors.length;
            }
            return flightVectors[returnId];
        }

        /* negative degrees will be reduced until they are between 0 and 360 degrees */
        if(degrees > flightVectors.length-1) {

            while(returnId > flightVectors.length-1) {
                returnId = returnId - flightVectors.length;
            }

            return flightVectors[returnId];
        }

        if(degrees >= 0 && degrees <= flightVectors.length-1) {
            return flightVectors[degrees];
        }

        return null;
    }
}
