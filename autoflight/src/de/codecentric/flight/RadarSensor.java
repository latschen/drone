package de.codecentric.flight;

import java.awt.*;

/**
 * Radar sensor scanning the area upfront in a given distance.
 * Will scan the upfront area in its own thread and provides the distance
 * as soon as a new value is available.
 */
public class RadarSensor implements Runnable {

    /* angle in degrees in which the sensor is mounted */
    private int sensorAngel;

    /* ms between radar refresh */
    private static long refreshDelay = 10;

    /* the drone where this sensor is adapted to */
    private AbstractDrone drone;

    /* maximum distance the radar scans for objects. configurable externally */
    private int maxRadarDistance;

    /* the last measured distance upfront */
    private int upfrontDistance = -1;

    /* only for painting. the radar length will be painted in different colors for every length in this list */
    private int[] distances;


    public RadarSensor(AbstractDrone drone, int maxRadarDistance, int sensorMountAngle, int[] distances) {
        this.distances = distances;
        this.sensorAngel = sensorMountAngle;
        this.drone = drone;
        this.maxRadarDistance = maxRadarDistance;

        Thread radarScanThread = new Thread(this);
        radarScanThread.start();
    }


    @Override
    public void run() {
        System.out.println("Starting radar sensor..");

        while(true) {
            performDistanceScan();

            try {
                Thread.sleep(refreshDelay);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * delivers the last measured distance value
     * @return the distance to the next obstascle upfront
     */
    public int getUpfrontDistance() {
        return upfrontDistance;
    }


    /**
     * paint the radar line. each distance will be painted in a different color.
     * @param g the Graphics to paint the line into
     * @param x the neutral X coordinate of the drone to start the lines from
     * @param y the neutral Y coordinate of the drone to start the lines from
     */
    protected void paintRadarLine(Graphics g, double x, double y) {

        FlightVector currentVector = getCurrentSensorVector();

        for(int i=0; i<distances.length; i++) {

            /* draw the radar area */
            if(i == 0) g.setColor(Color.green);
            if(i == 1) g.setColor(Color.orange);
            if(i >  1) g.setColor(Color.blue);

            if(i != 0) {
                g.drawLine(
                        (int) (x + currentVector.x * distances[i - 1]),
                        (int) (y + currentVector.y * distances[i - 1]),
                        (int) (x + currentVector.x * distances[i]),
                        (int) (y + currentVector.y * distances[i])
                );
            } else {
                g.drawLine(
                        (int) x,
                        (int) y,
                        (int) (x + currentVector.x * distances[i]),
                        (int) (y + currentVector.y * distances[i]));
            }
        }
    }


    /**
     * get the current absolute vector of the sensor
     * @return the current vector of this sensor
     */
    private FlightVector getCurrentSensorVector() {
        return FlightVector.getFlightVectorByAngle(drone.getCurrentFlightVector().degrees + sensorAngel);
    }


    /**
     * calculate the distance (in frontal direction) to the next bar
     */
    private void performDistanceScan() {

        int minimumFoundDistance = -1;

        FlightVector sensorVector = getCurrentSensorVector();

        for(int i=0; i<=maxRadarDistance; i++) {

            /* iterate over all existing obstacles ("obstacles") */
            for(Rectangle r : WorldPanel.obstacles) {

                Point radarPoint;

                radarPoint = new Point(
                        (int)(drone.getCurrentDroneCenterPosition().x + i*sensorVector.x),
                        (int)(drone.getCurrentDroneCenterPosition().y + i*sensorVector.y));

                if(r.contains(radarPoint)) {
                    /* if this is the first bar found upfront, or if this was nearer then the obstacles before, update */
                    if(minimumFoundDistance == -1 || i < minimumFoundDistance) {
                        minimumFoundDistance = i;
                    }
                }
            }
        }

        upfrontDistance = minimumFoundDistance;
    }
}
