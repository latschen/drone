package de.codecentric.flight;

import java.awt.*;

/**
 * Implement this interface to create your own drone
 */
public interface DroneInterface {

    /**
     * perform the general basic movement of the drone
     */
    void move();


    /**
     * turn clockwise by one degree
     * @param clockWise true if clockwise direction, otherwise false
     */
    void turnDirection(boolean clockWise);


    /**
     * change the speed of the drone by the given difference. consideres minimum and maximum speed
     * @param type
     */
    void changeDroneSpeed(int type);


    /**
     * change the speed of the drone in strafe direction
     * @param type
     */
    void changeDroneStrafeSpeed(int type);


    /**
     * get the current position of the drone (center of the drone)
     * @return
     */
    Point getCurrentDroneCenterPosition();


    /**
     * get the current forward speed of the drone
     * @return
     */
    double getDroneSpeed();


    /**
     * get the current strafe speed of the drone
     * @return
     */
    double getDroneStrafeSpeed();


    /**
     * perform the drone-specific movement
     */
    void moveDrone();


}
