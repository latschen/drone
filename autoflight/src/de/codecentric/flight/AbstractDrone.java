package de.codecentric.flight;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/***
 * complete this abstract drone with your own implementation and add some sensors
 */
public abstract class AbstractDrone extends JPanel implements Runnable, DroneInterface {

    /* === CONFIGURABLE DRONE PARAMETERS ========================================================================== */

    /* drone painting characteristics */
    private int droneSize = 20;

    /* the delay between two moves in ms. default : 10 */
    private long moveDelay = 10;

    /* min and max upfront speed of the drone. default : 0.2 and 1.0 */
    private double minDroneSpeed = 0.1;
    private double maxDroneSpeed = 1.2;

    /* mit and max strafe speed of the drone. default : -1.0 and 1.0 */
    private double minDroneStrafeSpeed = -1.0;
    private double maxDroneStrafeSpeed =  1.0;

    /* the interval per step the speed changes at breaking or accelerating. default : 0.01 */
    private double speedChangeInterval = 0.01;

    /* the interval per step the strafe speed changes. default : 0.02 */
    private double speedStrafeChangeInterval = 0.02;


    /* === FLIGHT BEHAVIOR ======================================================================================== */

    /* set true if the drone shut always perform big curves */
    protected boolean enableAutoCurve = true;

    /* change the auto curving on a random base sometimes */
    protected boolean changeAutoCurveSometimes = true;


    /* INTERNAL STUFF ============================================================================================= */

    /* the id ( = degrees) of the current flight vector (the vector where the drone head is looking in) */
    private int currentFlightVector = 0;

    /* the main rotation direction. */
    private boolean clockwise = false;

    /* current speed of the drone (see @maxDroneSpeed and @minDroneSpeed) in direction of @currentFlightVector */
    private double droneSpeed = minDroneSpeed;

    /* current speed of the drone in strafe direction (negative values = left, positive = right) */
    private double droneStrafeSpeed = 0.0;


    /* HEAVY TURNING RECOGNITION ================================================================================== */

    /* the timestamps of the last x movements */
    private ArrayList<Long> lastCurveTimestamps = new ArrayList<>();

    /* how many turns per time are needed to indicate "heavy turning"? */
    private int lastCurveHistorySize = 30;

    /* in which time does the turns have to accure to indicate "heavy turning"? */
    private long heavilyTurningIndicatorTime = 2500;

    /* hold the state if the drone is currently heavily turning */
    private boolean isHeavilyTurning = false;

    /* count how many curves there was in a row without upfront moving in between */
    private int curvesInARow = 0;

    /* list of all pre-calculated flight vectors for 360 degrees */
    private FlightVector[] flightVectors = FlightVector.flightVectors;

    /* list of all mounted sensors */
    private ArrayList<RadarSensor> sensors = new ArrayList<>();

    /* starting drone coordinates (center of the drone) */
    private double x;
    private double y;


    public AbstractDrone(double startX, double startY) {

        x = startX;
        y = startY;

        try {
            System.out.println("Starting drone..");
            Thread.sleep(1000);
        } catch(Exception e) {
            e.printStackTrace();
        }

        Thread t = new Thread(this);
        t.start();
    }


    /**
     * add a radar sensor to the sensor list
     * @param sensor the sensor to add
     */
    protected void addRadarSensor(RadarSensor sensor) {
        sensors.add(sensor);
    }


    @Override
    public void run() {

        long ct = 0;

        while(true) {
            try {
                ct++;
                Thread.sleep(moveDelay);

                if(ct % 10 == 0) performAutoCurve();
                recognizeHeavyTurning();
                move();
                changeAutoCurveSometimes();

            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * perform a change of the auto curve direction sometimes when this method is called
     */
    private void changeAutoCurveSometimes() {

        /* do not change if deactivated or if drone is heavily turning */
        if (!changeAutoCurveSometimes || isHeavilyTurning) return;

        if((int)(Math.random()*10000) % 500 == 0) {
            System.out.println("turning direction...");
            clockwise = !clockwise;
        }

    }


    /**
     * perform auto turning drone if activated
     */
    private void performAutoCurve() {

        if(enableAutoCurve) {
            turnDirection(clockwise);
        }
    }


    /**
     * recognize if the drone is turning heavily in the last time
     */
    private void recognizeHeavyTurning() {

        if(lastCurveTimestamps.size() < lastCurveHistorySize) {
            isHeavilyTurning = false;
        } else {
            if (lastCurveTimestamps.get(lastCurveHistorySize-1) > System.currentTimeMillis() - heavilyTurningIndicatorTime &&
                lastCurveTimestamps.get(0) > System.currentTimeMillis() - heavilyTurningIndicatorTime) {
                isHeavilyTurning = true;
            } else {
                isHeavilyTurning = false;
            }
        }
    }


    @Override
    public void paintComponent(Graphics g) {

        paintInfoBox(g);

        /* draw the drone as a circle with x/y in the center */
        g.setColor(new Color(255, 150, 150));
        g.fillOval((int)x-(droneSize/2), (int)y-(droneSize/2), droneSize, droneSize);

        for(RadarSensor sensor : sensors) {
            sensor.paintRadarLine(g, x, y);
        }
    }


    /**
     * paint a little info box with speed and things
     * @param g paint box into that graphics
     */
    private void paintInfoBox(Graphics g) {

        int startX = 910;
        int startY = 510;

        g.fillRect(startX, startY, 180, 230);

        g.setColor(Color.black);
        String speedString = "" + droneSpeed;
        String strafeSpeedString = "" + droneStrafeSpeed;
        String vectorX = ""+flightVectors[currentFlightVector].x;
        String vectorY = ""+flightVectors[currentFlightVector].y;


        if(speedString.length() > 4) speedString = speedString.substring(0, 4);
        if(strafeSpeedString.length() > 4) strafeSpeedString = strafeSpeedString.substring(0, 4);
        if(vectorX.length() > 4) vectorX = vectorX.substring(0, 4);
        if(vectorY.length() > 4) vectorY = vectorY.substring(0, 4);

        g.drawString("drone speed:  " + speedString, startX, startY+10);
        g.drawString("strafe speed: " + strafeSpeedString, startX, startY+30);
        g.drawString("vector: (" + flightVectors[currentFlightVector].degrees + "°) " + vectorX + " | " + vectorY, startX, startY+50);
        g.drawString("turns in a row: " + curvesInARow, startX, startY+70);

        if(isHeavilyTurning) {
            g.setColor(Color.red);
            g.fillOval(startX, startY+78, 10, 10);
            g.drawString("heavy turning!", startX + 18, startY+88);
        }




        /* draw a cross with the drone direction and speed visualized inside */
        int crossCenterX = startX + 90;
        int crossCenterY = startY + 170;

        int crossSize = 80;
        double speedPercentage = (droneSpeed / maxDroneSpeed);
        double strafePercentage = (droneStrafeSpeed / maxDroneStrafeSpeed);

        g.setColor(Color.yellow);
        g.drawLine(crossCenterX-crossSize, crossCenterY, crossCenterX+crossSize, crossCenterY);
        g.drawLine(crossCenterX, crossCenterY-crossSize, crossCenterX, crossCenterY+crossSize);

        /* draw drone vector */
        g.setColor(Color.cyan);

        int sX = crossCenterX + (int)(getCurrentFlightVector().strafeVector.x*crossSize*strafePercentage);
        int sY = crossCenterY + (int)(getCurrentFlightVector().strafeVector.y*crossSize*strafePercentage);
        int eX   = crossCenterX + (int)(getCurrentFlightVector().x*crossSize*speedPercentage + getCurrentFlightVector().strafeVector.x*crossSize*strafePercentage);
        int eY   = crossCenterY + (int)(getCurrentFlightVector().y*crossSize*speedPercentage + (getCurrentFlightVector().strafeVector.y*crossSize*strafePercentage));

        g.drawLine(sX, sY, eX, eY);
        g.fillOval(sX-5, sY-5, 10, 10);

    }


    /**
     * turn the drone in the currently active direction
     */
    protected void turnDirection() {
        turnDirection(clockwise);
    }


    /**
     * turn clockwise by x degrees
     * @param clockwise true if clockwise direction, otherwise false
     * @param degrees the amount of degrees to turn
     */
    protected void turnDirection(boolean clockwise, int degrees) {
        for(int i=0; i<degrees; i++) {
            turnDirection(clockwise);
        }
    }


    @Override
    public void turnDirection(boolean clockWise) {

        ++curvesInARow;
        addTurnTimestamp();

        if(clockWise) {

            if (currentFlightVector == (flightVectors.length - 1)) {
                currentFlightVector = 0;
            } else {
                currentFlightVector = currentFlightVector + 1;
            }
        }
        else {
            if(currentFlightVector == 0) {
                currentFlightVector = flightVectors.length-1;
            } else {
                currentFlightVector = currentFlightVector -1;
            }
        }
    }


    /**
     * keep a list of the timestamps of the last 100 turns
     */
    private void addTurnTimestamp() {

        lastCurveTimestamps.add(System.currentTimeMillis());

        if(lastCurveTimestamps.size() > lastCurveHistorySize) {
            lastCurveTimestamps.remove(0);
        }

    }


    /**
     *
     * @param mode FlightVector.BREAK or FlightVector.ACCELERATE
     */
    public void changeDroneSpeed(int mode) {

        if(mode != FlightVector.ACCELERATE && mode != FlightVector.BREAK)
            throw new IllegalArgumentException("\" " + mode + "\" is not a valid argument. " +
                    "please use FlightVector.BREAK or FlightVector.ACCELERATE instead.");

        double diff = speedChangeInterval;

        if(mode == FlightVector.BREAK) diff = speedChangeInterval * -1;

        if(droneSpeed + diff >= maxDroneSpeed) {
            droneSpeed = maxDroneSpeed;
            return;
        }
        if(droneSpeed + diff <= minDroneSpeed)   {
            droneSpeed = minDroneSpeed;
            return;
        }

        droneSpeed = droneSpeed + diff;
    }


    /**
     * change strafe speed in left or right
     * @param type FlightVector.DIRECTION_LEFT or FlightVector.DIRECTION_RIGHT
     */
    public void changeDroneStrafeSpeed(int type) {

        if(type!= FlightVector.DIRECTION_LEFT && type != FlightVector.DIRECTION_RIGHT)
            throw new IllegalArgumentException("\" " + type + "\" is not a valid argument. " +
                    "please use FlightVector.DIRECTION_LEFT or FlightVector.DIRECTION_RIGHT instead.");

        double diff;

        if(type == FlightVector.DIRECTION_LEFT) diff = speedStrafeChangeInterval;
        else                                    diff = speedStrafeChangeInterval * -1;

        if(droneStrafeSpeed + diff >= maxDroneStrafeSpeed) {
            droneStrafeSpeed = maxDroneStrafeSpeed;
            return;
        }
        if(droneStrafeSpeed + diff <= minDroneStrafeSpeed)   {
            droneStrafeSpeed= minDroneStrafeSpeed;
            return;
        }

        droneStrafeSpeed = droneStrafeSpeed + diff;
    }


    protected FlightVector getCurrentFlightVector() {
        return flightVectors[currentFlightVector];
    }


    @Override
    public Point getCurrentDroneCenterPosition() {
        return new Point((int)x,(int)y);
    }


    /**
     * breaks the strafing impulse from left AND right
     */
    protected void breakStrafing() {

        if(droneStrafeSpeed == 0) return;

        if(droneStrafeSpeed > 0) droneStrafeSpeed = droneStrafeSpeed - speedChangeInterval;
        else  droneStrafeSpeed = droneStrafeSpeed + speedChangeInterval;
    }


    /**
     * slow down the strafing impulse only for a given direction
     * @param direction FlightVector.DIRECTION_LEFT or FlightVector.DIRECTION_RIGHT
     */
    protected void breakStrafing(int direction) {

        if(direction != FlightVector.DIRECTION_LEFT && direction != FlightVector.DIRECTION_RIGHT)
            throw new IllegalArgumentException("\" " + direction + "\" is not a valid argument. " +
                    "please use FlightVector.DIRECTION_LEFT or FlightVector.DIRECTION_RIGHT instead.");

        if(droneStrafeSpeed == minDroneStrafeSpeed) return;

        if(droneStrafeSpeed > 0 && direction == FlightVector.DIRECTION_LEFT && droneStrafeSpeed > minDroneStrafeSpeed) {
            droneStrafeSpeed = droneStrafeSpeed - speedChangeInterval;
            return;
        }


        if(droneStrafeSpeed < 0 && direction == FlightVector.DIRECTION_RIGHT && droneStrafeSpeed < maxDroneStrafeSpeed) {
            droneStrafeSpeed = droneStrafeSpeed + speedChangeInterval;
            return;
        }
    }


    public void move() {

        moveDrone();

    }


    protected void resetCurvesInARow() {
        curvesInARow = 0;
    }


    @Override
    public double getDroneSpeed() {
        return droneSpeed;
    }


    @Override
    public double getDroneStrafeSpeed() {
        return droneStrafeSpeed;
    }


    /**
     * move the drone by the given distances
     * @param plusX the distance in X to move
     * @param plusY the distance in Y to move
     */
    protected void moveDrone(double plusX, double plusY) {
        x = x + plusX;
        y = y + plusY;

    }


    protected void setMaxDroneSpeed(double maxDroneSpeed) {
        this.maxDroneSpeed = maxDroneSpeed;
    }

    protected void setMinDroneSpeed(double minDroneSpeed) {
        this.minDroneSpeed = minDroneSpeed;
    }



    abstract public void moveDrone();
}

