package de.codecentric.flight.drones;

import de.codecentric.flight.AbstractDrone;
import de.codecentric.flight.FlightVector;
import de.codecentric.flight.RadarSensor;

/**
 * drone implementation with two radar sensors
 */
public class TwoSensorDrone extends AbstractDrone {


    /* the distance when the drone will react by turning into another direction */
    private int reactionDistance = 120; // default 150

    /* distance when the drone will start to slow down */
    private int slowDownDistance = 160; // default 200

    /* mounted sensors */
    private RadarSensor leftRadarSensor;
    private RadarSensor rightRadarSensor;


    public TwoSensorDrone(double startX, double startY) {

        super(startX, startY);

        setMinDroneSpeed(0.0);
        enableAutoCurve = false;
        changeAutoCurveSometimes = false;

        int[] strafeDistances = {reactionDistance};

        leftRadarSensor  = new RadarSensor(this, 100, -45, strafeDistances);
        rightRadarSensor = new RadarSensor(this, 100, 45, strafeDistances);

        addRadarSensor(leftRadarSensor);
        addRadarSensor(rightRadarSensor);

    }



    @Override
    public void moveDrone() {

        int distLeft = leftRadarSensor.getUpfrontDistance();
        int distRight = rightRadarSensor.getUpfrontDistance();

        if((distLeft < slowDownDistance && distLeft != -1) || (distRight < slowDownDistance && distRight != -1)) {
            changeDroneSpeed(FlightVector.BREAK);
        } else {
            changeDroneSpeed(FlightVector.ACCELERATE);
        }


        /* upfront is free */
        if(distLeft > reactionDistance && distRight > reactionDistance) {
            resetCurvesInARow();
            breakStrafing();
        } else {

            if(distLeft < reactionDistance && distRight < reactionDistance && distLeft != -1 && distRight != -1) {
                changeDroneStrafeSpeed(FlightVector.DIRECTION_LEFT);
            }

            if(distLeft < reactionDistance && distRight >= reactionDistance) {
                turnDirection(true);
            }

            if(distRight < reactionDistance && distLeft >= reactionDistance) {
                turnDirection(false);
            }
        }


        /* upfront direction */

        double moveX = getCurrentFlightVector().x * getDroneSpeed();
        double moveY = getCurrentFlightVector().y * getDroneSpeed();

        moveDrone(moveX, moveY);

        /* strafe direction */

        double strafeX = getCurrentFlightVector().strafeVector.x * getDroneStrafeSpeed();
        double strafeY = getCurrentFlightVector().strafeVector.y * getDroneStrafeSpeed();

        moveDrone(strafeX, strafeY);

        /* STRAFING BEHAVIOR */
//
//        if(distLeft != -1 && distLeft < reactionDistance) {
//
//
//            turnDirection(true);
//        } else {
//            breakStrafing(FlightVector.DIRECTION_LEFT);
//        }
//
//        if(distRight != -1 && distRight < reactionDistance) {
//            changeDroneStrafeSpeed(FlightVector.DIRECTION_RIGHT);
//            turnDirection(false);
//        } else {
//            breakStrafing(FlightVector.DIRECTION_RIGHT);
//        }
//
//        // no obstacle left AND right
//        if((distRight == -1 || distRight > reactionDistance) && (distLeft == -1 || distLeft > reactionDistance)) {
//            breakStrafing();
//        }
    }
}
