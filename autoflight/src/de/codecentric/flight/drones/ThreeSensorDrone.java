package de.codecentric.flight.drones;

import de.codecentric.flight.AbstractDrone;
import de.codecentric.flight.FlightVector;
import de.codecentric.flight.RadarSensor;

/**
 * drone implementation with three radar sensors
 */
public class ThreeSensorDrone extends AbstractDrone {


    /* the distance when the drone will react by turning into another direction */
    private int reactionDistance = 120; // default 150

    /* distance when the drone will start to slow down */
    private int slowDownDistance = 160; // default 200

    /* distance when the drone will start strafing left or right */
    private int reactionDistanceSide = 50;

    /* mounted sensors */
    private RadarSensor radarSensor;
    private RadarSensor leftRadarSensor;
    private RadarSensor rightRadarSensor;


    public ThreeSensorDrone(double startX, double startY) {

        super(startX, startY);

        int[] distances = {reactionDistance, slowDownDistance};
        int[] strafeDistances = {reactionDistanceSide};

        radarSensor      = new RadarSensor(this, slowDownDistance + 10, 0, distances);
        leftRadarSensor  = new RadarSensor(this, 100, -30, strafeDistances);
        rightRadarSensor = new RadarSensor(this, 100, 30, strafeDistances);

        addRadarSensor(radarSensor);
        addRadarSensor(leftRadarSensor);
        addRadarSensor(rightRadarSensor);

    }



    @Override
    public void moveDrone() {

        int distUpfront = radarSensor.getUpfrontDistance();

        /* slow down drone if obstacle in slow down distance */
        if(distUpfront != -1 && distUpfront < slowDownDistance) {
            changeDroneSpeed(FlightVector.BREAK);
        } else {
            changeDroneSpeed(FlightVector.ACCELERATE);
        }

        int distLeft  = leftRadarSensor.getUpfrontDistance();
        int distRight = rightRadarSensor.getUpfrontDistance();

        /* free way upfront -> move */
        if(distUpfront == -1 || distUpfront > reactionDistance) {
            resetCurvesInARow();
        } else {
            // TODO seems double:
//            changeDroneSpeed(FlightVector.BREAK);
            turnDirection();
        }

        /* upfront direction */

        double moveX = getCurrentFlightVector().x * getDroneSpeed();
        double moveY = getCurrentFlightVector().y * getDroneSpeed();

        moveDrone(moveX, moveY);

        /* strafe direction */

        double strafeX = getCurrentFlightVector().strafeVector.x * getDroneStrafeSpeed();
        double strafeY = getCurrentFlightVector().strafeVector.y * getDroneStrafeSpeed();

        moveDrone(strafeX, strafeY);

        /* STRAFING BEHAVIOR */

        if(distLeft != -1 && distLeft < reactionDistanceSide) {
            changeDroneStrafeSpeed(FlightVector.DIRECTION_LEFT);

            turnDirection(true);
        } else {
            breakStrafing(FlightVector.DIRECTION_LEFT);
        }

        if(distRight != -1 && distRight < reactionDistanceSide) {
            changeDroneStrafeSpeed(FlightVector.DIRECTION_RIGHT);
            turnDirection(false);
        } else {
            breakStrafing(FlightVector.DIRECTION_RIGHT);
        }

        // no obstacle left AND right
        if((distRight == -1 || distRight > reactionDistanceSide) && (distLeft == -1 || distLeft > reactionDistanceSide)) {
            breakStrafing();
        }
    }
}
