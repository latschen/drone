package de.codecentric.flight;

import javax.swing.*;

/**
 * Starter for the 2d drone simulator
 */
public class Start {

    //TODO: erkennen, ob die drohne "in der enge" ist. dann kurvenwechsel verhindern, oder erzwingen
    //TODO improvement: wenn es in kurzer zeit viele richtungswechsel gibt, die radar-distanz reduzieren
    //TODO improvement: anzahl der durchgeführten kurven als indikator für einen zu großen radar
    //TODO: notfallreaktion: erkennt die drohne, dass sie extrem nah an einer wand steht (z.b. beim wenden), sofort "step backwards"

    public static void main(String[] args) {

        JFrame frame = new JFrame("Auto move test");
        frame.setSize(WorldPanel.WORLD_SIZE_X, WorldPanel.WORLD_SIZE_Y);

        frame.setContentPane(new WorldPanel());
        frame.setVisible(true);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
